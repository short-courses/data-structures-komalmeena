from numpy import *

rows = int(input("Enter the number of rows: "))
columns = int(input("Enter the number of columns: "))

matrixOne = zeros((rows, columns))
matrixTwo = zeros((rows, columns))

# Input values for matrixOne
print("Enter values for matrixOne:")
for i in range(rows):
    for j in range(columns):
        matrixOne[i][j] = input(f"Enter element at matrixOne[{i + 1}][{j + 1}]: ")

# Input values for matrixTwo
print("Enter values for matrixTwo:")
for i in range(rows):
    for j in range(columns):
        matrixTwo[i][j] = input(f"Enter element at matrixTwo[{i + 1}][{j + 1}]: ")

# Print the matrices
print("Matrix One:")
print(matrixOne, '\n')

print("Matrix Two:")
print(matrixTwo, '\n')

# Add
print('Sum of two matrices:')
print(add(matrixOne, matrixTwo))