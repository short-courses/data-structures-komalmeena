class PriorityLane:
    def __init__(self):
        self.items = []

    def enqueue(self, item):
        self.items.append(item)

    def dequeue(self):
        if not self.is_empty():
            return self.items.pop(0)
        else:
            raise IndexError("Queue is empty")

    def is_empty(self):
        return not self.items

    def peek(self):
        if self.items:
            return self.items[0]
        else:
            raise IndexError("Queue is empty")

    def size(self):
        return len(self.items)

    def print_queue(self):
        print(self.items)


# Create an object from the PriorityLane class called "security"
security = PriorityLane()

# Add the following items to the 'security' list
security.enqueue("senior")
security.enqueue("pregnant")
security.enqueue("PWD")
security.enqueue("normal person")

security.print_queue()

# Check if the 'security' list is currently empty
print("Is 'security' list empty?", security.is_empty(), '\n')

# Get the size or length of the 'security' list
print("The current size of security list:", security.size(), '\n')

# get the rear item
print("Peek of the new list queue item:", security.peek(), '\n')

# Remove the front item of the 'security' list
print("The item is removed from the security list:", security.dequeue(), '\n')